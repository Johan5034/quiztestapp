﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class DataController : MonoBehaviour
{
    private GameData gameData;
    public QuestionData QuestionData;

    private static DataController instance;
    public static DataController Instance
    {
        get
        {
            if (instance == null)
            {
                Create();
            }
            return instance;
        }
    }
    private static void Create()
    {
        GameObject go = new GameObject("DataController");
        go.AddComponent<DataController>();
    }
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

        }
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }
    private void Start()
    {
        // DontDestroyOnLoad(gameObject);
        LoadGameData(1);
    }
    public GameData GetCurrentdata(int index)
    {
        LoadGameData(index);
        return gameData;
    }

    private void LoadGameData(int index)
    {      
        //string gameDataProjectFilePath = "/StreamingAssets/";
        //string filePath = Path.Combine(Application.dataPath,gameDataFileName);
        string filePath = Application.dataPath + "/StreamingAssets/Question" + index.ToString() + ".json";
    
        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            gameData = JsonUtility.FromJson<GameData>(dataAsJson);

        }
        else
        {
            Debug.Log("No Data Found");
            return;
        }

    }
}
