﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AnswerButton : MonoBehaviour 
{
	public Text answerText;

	//private GameController gameController;
    QuizManager quizManager;
	private AnswerData answerData;

	void Start()
	{
		//gameController = FindObjectOfType<GameController>();
        quizManager = FindObjectOfType<QuizManager>();
	}

	public void SetUp(AnswerData data)
	{
		answerData = data;
		answerText.text = answerData.answerText;
	}

  
	public void HandleClick()
	{
        quizManager.AnswerButtonClicked(answerData.isCorrect);
		//gameController.AnswerButtonClicked(answerData.isCorrect);
	}
}
